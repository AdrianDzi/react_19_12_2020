import React from 'react';

const Loader = () => {
    return (
        <div className="alert alert-info"> ładuje dane...</div>
    )
}

export default Loader;