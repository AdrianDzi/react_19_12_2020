import React from 'react';

const ShowError = () => {
    return (
        <div className="alert alert-info"> Error</div>
    )
}

export default ShowError;