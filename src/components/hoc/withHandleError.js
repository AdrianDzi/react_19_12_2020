import React from 'react';
import ShowError from '../../ShowError';

const withHandleError = Comp => {
    class withHandleErrorClass extends React.Component {


        render() {
            const { error, ...restProps } = this.props; //może miec {error: '', books: [], loading: false)

            if (error) {
                return <ShowError />;
            }

            return <Comp {...restProps} />
        }
    }
    withHandleErrorClass.displayName = `withHandleError(${Comp.displayName || Comp.name})`
    return withHandleErrorClass;
}
export default withHandleError;