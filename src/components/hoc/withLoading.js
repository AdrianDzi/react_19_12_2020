import React from 'react';
import Loader from '../../Loader';

const withLoading = Comp => {
    class withLoadingClass extends React.Component {

        render() {
            const { Loading, ...restProps } = this.props; //może miec {error: '', books: [], loading: false)

            if (Loading) {
                return <Loader />;
            }

            return <Comp {...restProps} />
        }
    }
    withLoadingClass.displayName = `withLoading(${Comp.displayName || Comp.name})`
    return withLoadingClass;
}
export default withLoading;