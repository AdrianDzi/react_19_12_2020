import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import IndexContainer from './IndexContainer'
import Nav from './Nav';
import About from './about';
import Contact from './contact';
import Index from './Index';
import BookDetailContainer from './BookDetail/BookDetailContainer';

class App extends React.Component {

    render() {

        return (
            <React.Fragment>
                <Router>
                    <Nav />
                    <div className="container" >

                        <Switch>
                            <Route path="/" exact component={IndexContainer} />
                            <Route path="/about" exact component={About} />
                            <Route path="/contact" exact component={Contact} />
                            <Route path="/book/:key/:id" exact component={BookDetailContainer} />
                        </Switch>

                    </div>
                </Router>
            </React.Fragment>
        );
    }
}

export default App;