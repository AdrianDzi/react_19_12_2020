import React from 'react';
import { compose } from 'redux';
import olApi from '../olApi';
import SearchForm from './SearchForm';
import BookList from './BookList'
import { withHandleError, withLoading } from './hoc/index';
import BooksFilterContainer from './BookFilter/BookFilterContainer';

const BookListWithHandleError = compose(withHandleError, withLoading)(BookList);
const BookListWithHandleErrorAndLoading = withLoading(BookListWithHandleError);

class Index extends React.Component {

    state = { bookList: [], bookLoading: false, error: '' };

    onSearchFormSubmit = (term) => {
        console.log("wyszukuje: ", term);
        // this.setState({ bookLoading: true });
        olApi.get('search.json', {
            params: { title: term }
        }).then((response) => {
            this.props.booksFetched(response.data.docs);
            console.log(response.data.docs);
        }).catch((error) => {
                // this.setState({ error: 'Nie udało się pobrać książek', bookLoading: false });
            }
        );
    }
    render() {
        return (
            <div>
                <div className="mt-3 pb-0 alert alert-secondary">
                    <SearchForm onFormSubmit={this.onSearchFormSubmit}></SearchForm>
                </div>
                <BooksFilterContainer />
                <BookListWithHandleErrorAndLoading list={this.props.books} />
                
            </div>
        );
    }
}
export default Index;