import { React, useEffect, useState } from 'react';
import BookDetail from './BookDetail';
import olApi from '../../olApi';

const BookDetailContainer = (props) => {
  const [book, setBook] = useState({});

  // componentDidMount() {
  //   axios.get(`http://openlibrary.org/${this.props.match.params.key}/${this.props.match.params.id}.json`).then((response) => {
  //     this.setState({ book: response.data });
  //   });
  // }

  useEffect(() => {
    olApi(`${this.props.match.params.key}/${this.props.match.params.id}.json`)
      .then((response) => {
        setBook({ book: response.data });
      });
      return() => {
        console.log("papa!");
      }
  }, [props.match.params.key, props.match.params.id]);

  return (
    <BookDetail book={book}>
    </BookDetail>
  );

}

export default BookDetailContainer;