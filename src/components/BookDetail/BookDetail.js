import React from 'react';

const BookDetail = (props) => {
  return (
    <div>
      <h1><b>{props.book.title}</b></h1>
      <p>{props.book.description}</p>
    </div>
  )
}

export default BookDetail;