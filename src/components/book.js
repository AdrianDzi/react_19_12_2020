import React from 'react';
import { Link } from 'react-router-dom';

function Book(props) {
        return (
            <div className="row">
                <div className="col-2">
                    <img src={'http://covers.openlibrary.org/b/id/' + props.cover + '-S.jpg'} alt="" className="img-fluid"></img>
                </div>
                <div>
                    <div className="col-10">
                        <h2><b><Link to={`/book${props._key}`}>{props.title}</Link></b></h2>
                        <p>{props.author}</p>
                        <p>{props.first_publish_year}</p>
                    </div>
                </div>

            </div>
        );
    }

export default Book;