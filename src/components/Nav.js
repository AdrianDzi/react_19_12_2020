import React from 'react';
import { Link } from 'react-router-dom';

function Nav() {
        return (
            <nav className="navbar navbar-expand-lg navbar-dark bg-info">
                <div className="container">
                    <Link className="navbar-brand" to="/">księgarnia</Link>
                    <div className="navBar-nav">
                        <Link className="nav-item nav-Link" to="/about">o nas</Link>
                        <Link className="nav-item nav-Link" to="/contact">Kontakt</Link>
                    </div>
                </div>
            </nav>
        );
    }

export default Nav;