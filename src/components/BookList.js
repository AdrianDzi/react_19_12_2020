import React from 'react';
import Book from './book';

function BookList(props) {
    const list = props.list.map(book =>
        
        <Book book={book} _key={book.key} cover={book.cover_i} title={book.title} author={book.author_name} first_publish_year={book.first_publish_year} />
    );
    console.log(props);
    return (
        <div className="container">
            {list}
        </div>
    );
}

export default BookList;